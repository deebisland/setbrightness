#!/usr/bin/python3

from tkinter import *
import os

def setBrightness(screen, value, position, mode="1920x1080"):
    result = os.popen('xrandr --query | grep ^H').read()
    result = result.split()[2].split('x')[0]
    if result != '1920':
        os.system("xrandr --output {} --mode {} {} && sleep 2".format(screen, mode, position))
    os.system("xrandr --output {} --brightness {}".format(screen, value))
    if value == 0:
        os.system("xrandr --output {} --off".format(screen))
    exit()

window = Tk()

window.title("Select Brightness Level")

row_screen = ["HDMI-A-0", "eDP"]
position = ["--left-of eDP", "--right-of HDMI-A-0"]

for row in range(2):
    for column in range(11):
        btn = Button(window, text=" {}%".format(column * 10), command=lambda column=column, screen=row_screen[row], position=position[row]: setBrightness(screen, column / 10, position))
        btn.grid(column=column + 1, row=row)

btn = Button(window, text="Mirror Screen", command=lambda : os.system("xrandr --output HDMI-A-0 --same-as eDP"))
btn.grid(column=1, row=2, columnspan=2)

window.mainloop()
